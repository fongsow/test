/**
 * Created by s17465 on 1/12/2016.
 */
(function () {
  angular
    .module("weddingGramApp")
    .controller("SettingCtrl", ["$scope", MyCtrl]);

  function MyCtrl($scope) {
    var self = this;
    self.myLists = [1];

    self.updateList = function () {
      console.log("abc");
      self.myLists = [1, 2, 3, 4, 5];
      $scope.$broadcast('scroll.refreshComplete')
    }

  }
})();
