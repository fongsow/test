(function () {
  angular
    .module("weddingGramApp")
    .controller("PostListCtrl", ["PostAPI", "$http", "$ionicActionSheet", "$scope", "SignUpModal", PostListCtrl]);

  function PostListCtrl(PostAPI, $http, $ionicActionSheet, $scope, SignUpModal) {
    var self = this;

    PostAPI
      .me()
      .then(function (response) {
        self.posts = response.data;
      })
      .catch(function (err) {
        console.log(err);
      });

    self.like = function (post) {

    };

    self.showActionSheet = function () {
      console.log("click");

      $ionicActionSheet.show({
        buttons: [
          {text: 'Facebook'},
          {text: 'Twitter'}
        ],
        titleText: 'Shares',
        cancelText: 'Cancel',
        cancel: function () {
          // add cancel code..
          console.log("cancel");
        },
        buttonClicked: function (index) {
          //check index for certain action:
          //if (index == 0) {}
          console.log(index);
          return true;
        },
        destructiveButtonClicked: function () {
          console.log("destructive");
          return true;
        }
      });


    };

    self.LikeButton = function() {
     SignUpModal.init($scope)
       .then(function(md) {md.show();})
    };

  }
})();
